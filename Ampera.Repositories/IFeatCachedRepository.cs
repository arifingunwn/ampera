﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ampera.Repositories
{
    public interface IFeatCachedRepository
    {
        public Task<Ampera.Entity.Feat.ApiListResponse> getApiList();
        public Task<Ampera.Entity.Feat.DaftarBarangResponse> getDaftarBarang(DateTime akhir);
        public Task<string> getRawReportById(int id);
        public Task<Ampera.Entity.Feat.SalesDailyReportResponse> getSalesDailyReport(DateTime awal, DateTime akhir);
        public Task<Ampera.Entity.Feat.LaporanRugiLabaProdukSTSReponse> getLaporanRugiLabaProdukSTS(DateTime awal, DateTime akhir);
        public Task<Ampera.Entity.Feat.LaporanPembayaranPiutangDetailResponse> getLaporanPembayaranPiutangDetail(DateTime awal, DateTime akhir);
    }
}
