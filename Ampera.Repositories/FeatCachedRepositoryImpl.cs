﻿using Ampera.Entity.Feat;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ampera.Repositories
{
    public class FeatCachedRepositoryImpl : IFeatCachedRepository
    {
        private readonly IMemoryCache _cache;
        private readonly IFeatRepository _featRepository;
        private readonly TimeSpan CACHE_EXPIRATION = TimeSpan.FromHours(1);
        public FeatCachedRepositoryImpl(IMemoryCache memoryCache, IFeatRepository featRepository)
        {
            _cache = memoryCache;
            _featRepository = featRepository;
        }

        public async Task<ApiListResponse> getApiList()
        {
            string cacheKey = "apilist";
            ApiListResponse result = null;
            if (_cache.TryGetValue(cacheKey, out result))
            {
                return result;
            }

            result = await _featRepository.getApiList();
            _cache.Set(cacheKey, result, CACHE_EXPIRATION);

            return result;
        }

        public async Task<DaftarBarangResponse> getDaftarBarang(DateTime akhir)
        {
            string cacheKey = "daftarbarang";
            DaftarBarangResponse result = null;
            if(_cache.TryGetValue(cacheKey, out result))
            {
                return result;
            }

            result = await _featRepository.getDaftarBarang(akhir);
            _cache.Set(cacheKey, result, CACHE_EXPIRATION);

            return result;
        }

        public async Task<LaporanPembayaranPiutangDetailResponse> getLaporanPembayaranPiutangDetail(DateTime awal, DateTime akhir)
        {
            return await _featRepository.getLaporanPembayaranPiutangDetail(awal, akhir);
        }

        public async Task<LaporanRugiLabaProdukSTSReponse> getLaporanRugiLabaProdukSTS(DateTime awal, DateTime akhir)
        {
            return await _featRepository.getLaporanRugiLabaProdukSTS(awal, akhir);
        }

        public Task<string> getRawReportById(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<SalesDailyReportResponse> getSalesDailyReport(DateTime awal, DateTime akhir)
        {
            return await _featRepository.getSalesDailyReport(awal, akhir);
        }
    }
}
