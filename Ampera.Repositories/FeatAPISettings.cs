﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ampera.Repositories
{
    public class FeatAPISettings
    {
        public string APIUrl { get; set; }
        public string APISecret { get; set; }
        public int ApiListApiId { get; set; }
        public int DaftarBarangApiId { get; set; }
        public int SalesDailyReportApiId { get; set; }
        public int LaporanRugiLabaProdukSTSApiId { get; set; }
        public int LaporanPembayaranPiutangDetail { get; set; }
    }
}
