﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;

namespace Ampera.Repositories
{
    public interface IModalTambahanRepository
    {
        public IEnumerable<Ampera.Entity.ModalTambahan> getAll();
        public bool createOrUpdate(string kodeBarang, decimal nilaiTambahan, DateTime updatedDate);
    }
}
