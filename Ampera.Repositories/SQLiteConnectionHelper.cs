﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ampera.Repositories
{
    public static class SQLiteConnectionHelper
    {
        public static SQLiteConnection createConnection()
        {
            SQLiteConnection connection = new SQLiteConnection("Data Source=database.db; Version=3;New=True;Compress=True");

            try
            {
                connection.Open();
            } catch(Exception ex)
            {
                   
            }

            return connection;
        }
    }
}
