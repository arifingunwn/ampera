﻿using Ampera.Entity.Feat;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ampera.Repositories
{
    public class FeatRepositoryImpl : IFeatRepository
    {
        private readonly FeatAPISettings _featAPISettings;

        public FeatRepositoryImpl(IOptions<FeatAPISettings> featAPISettings)
        {
            _featAPISettings = featAPISettings.Value;
        }

        private RestRequest generateRequest()
        {
            var request = new RestRequest();
            request.AddHeader("Authorization", _featAPISettings.APISecret);
            return request;
        }

        public async Task<ApiListResponse> getApiList()
        {
            var client = new RestClient(_featAPISettings.APIUrl + "?list=" + _featAPISettings.ApiListApiId.ToString());
            var request = generateRequest();

            var response = await client.ExecuteAsync(request);
            if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
            {
                var redirectedClient = new RestClient(response.ResponseUri.ToString());
                var redirectedResponse = await redirectedClient.ExecuteAsync(request);
                return JsonConvert.DeserializeObject<ApiListResponse>(redirectedResponse.Content);
            }

            return null;
        }

        public Task<string> getRawReportById(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<DaftarBarangResponse> getDaftarBarang(DateTime akhir)
        {
            var client = new RestClient(_featAPISettings.APIUrl + "?id=" + _featAPISettings.DaftarBarangApiId.ToString());
            client.AddDefaultQueryParameter("akhir", akhir.ToString("yyyyMMdd"));
            var request = generateRequest();

            var response = await client.ExecuteAsync(request);
            if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
            {
                var redirectedClient = new RestClient(response.ResponseUri.ToString());
                var redirectedResponse = await redirectedClient.ExecuteAsync(request);
                return JsonConvert.DeserializeObject<DaftarBarangResponse>(redirectedResponse.Content);
            }

            return null;
        }

        public async Task<SalesDailyReportResponse> getSalesDailyReport(DateTime awal, DateTime akhir)
        {
            var client = new RestClient(_featAPISettings.APIUrl + "?id=" + _featAPISettings.SalesDailyReportApiId.ToString());
            client.AddDefaultQueryParameter("awal", awal.ToString("yyyyMMdd"));
            client.AddDefaultQueryParameter("akhir", akhir.ToString("yyyyMMdd"));
            var request = generateRequest();

            var response = await client.ExecuteAsync(request);
            if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
            {
                var redirectedClient = new RestClient(response.ResponseUri.ToString());
                var redirectedResponse = await redirectedClient.ExecuteAsync(request);
                return JsonConvert.DeserializeObject<SalesDailyReportResponse>(redirectedResponse.Content, new JsonSerializerSettings()
                {
                    Culture = new System.Globalization.CultureInfo("id_ID"),
                    NullValueHandling = NullValueHandling.Ignore
                });
            }

            return null;
        }

        public async Task<LaporanRugiLabaProdukSTSReponse> getLaporanRugiLabaProdukSTS(DateTime awal, DateTime akhir)
        {
            var client = new RestClient(_featAPISettings.APIUrl + "?id=" + _featAPISettings.LaporanRugiLabaProdukSTSApiId.ToString());
            client.AddDefaultQueryParameter("awal", awal.ToString("yyyyMMdd"));
            client.AddDefaultQueryParameter("akhir", akhir.ToString("yyyyMMdd"));
            var request = generateRequest();

            var response = await client.ExecuteAsync(request);
            if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
            {
                var redirectedClient = new RestClient(response.ResponseUri.ToString());
                var redirectedResponse = await redirectedClient.ExecuteAsync(request);
                return JsonConvert.DeserializeObject<LaporanRugiLabaProdukSTSReponse>(redirectedResponse.Content, new JsonSerializerSettings()
                {
                    Culture = new System.Globalization.CultureInfo("id_ID"),
                    NullValueHandling = NullValueHandling.Ignore
                });
            }

            return null;
        }

        public async Task<LaporanPembayaranPiutangDetailResponse> getLaporanPembayaranPiutangDetail(DateTime awal, DateTime akhir)
        {
            var client = new RestClient(_featAPISettings.APIUrl + "?id=" + _featAPISettings.LaporanPembayaranPiutangDetail.ToString());
            client.AddDefaultQueryParameter("awal", awal.ToString("yyyyMMdd"));
            client.AddDefaultQueryParameter("akhir", akhir.ToString("yyyyMMdd"));
            var request = generateRequest();

            var response = await client.ExecuteAsync(request);
            if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
            {
                var redirectedClient = new RestClient(response.ResponseUri.ToString());
                var redirectedResponse = await redirectedClient.ExecuteAsync(request);
                return JsonConvert.DeserializeObject<LaporanPembayaranPiutangDetailResponse>(redirectedResponse.Content, new JsonSerializerSettings()
                {
                    Culture = new System.Globalization.CultureInfo("id_ID"),
                    NullValueHandling = NullValueHandling.Ignore
                });
            }

            return null;
        }
    }
}
