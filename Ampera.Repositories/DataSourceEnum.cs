﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ampera.Repositories
{
    public enum DataSourceEnum
    {
        Cache = 0,
        API = 1
    }
}
