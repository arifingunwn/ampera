﻿using Ampera.Entity;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ampera.Repositories
{
    public class ModalTambahanRepositoryImpl : IModalTambahanRepository
    {
        public bool createOrUpdate(string kodeBarang, decimal nilaiTambahan, DateTime updatedDate)
        {
            try
            {
                using (var connection = SQLiteConnectionHelper.createConnection())
                {
                    using (var command = connection.CreateCommand())
                    {
                        command.CommandText = @"
                        REPLACE INTO ModalTambahan (KodeBarang, NilaiTambahan, UpdatedDate) VALUES (@kodeBarang, @nilaiTambahan, @updatedDate)
                        ";
                        command.Parameters.AddWithValue("@kodeBarang", kodeBarang);
                        command.Parameters.AddWithValue("@nilaiTambahan", nilaiTambahan);
                        command.Parameters.AddWithValue("@updatedDate", updatedDate);
                        command.Prepare();

                        return command.ExecuteNonQuery() > 0;
                    }
                }

                return false;
            }
            catch (Exception  ex)
            {
                throw new Exception("Failed to execute Query", ex);
            }
        }

        public IEnumerable<ModalTambahan> getAll()
        {
            using (var connection = SQLiteConnectionHelper.createConnection())
            {
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = @"
                        SELECT * FROM ModalTambahan
                        ";

                    SQLiteDataReader dataReader = command.ExecuteReader();
                    while (dataReader.Read())
                    {
                        yield return new ModalTambahan
                        {
                            kodeBarang = dataReader["kodeBarang"].ToString(),
                            nilaiTambahan = Convert.ToDecimal(dataReader["nilaiTambahan"]),
                            updatedDate = Convert.ToDateTime(dataReader["updatedDate"])
                        };
                    }
                }
            }
        }
    }
}
