﻿using RestSharp;
using Newtonsoft.Json;
using Ampera.Entity.Feat;
using Ampera.Repositories;

namespace Ampera.Services
{
    public class FeatServiceImpl : IFeatService
    {
        private readonly IFeatCachedRepository _featRepository;
        public FeatServiceImpl(IFeatCachedRepository featRepository)
        {
            _featRepository = featRepository;
        }

        public async Task<ApiListResponse> getApiList()
        {
            return await _featRepository.getApiList();
        }

        public async Task<string> getRawReportById(int id)
        {
            return await _featRepository.getRawReportById(id);
        }

        public async Task<DaftarBarangResponse> getDaftarBarang(DateTime akhir)
        {
            return await _featRepository.getDaftarBarang(akhir);
        }

        public async Task<SalesDailyReportResponse> getSalesDailyReport(DateTime awal, DateTime akhir)
        {
            return await _featRepository.getSalesDailyReport(awal, akhir);
        }

        public async Task<LaporanRugiLabaProdukSTSReponse> getLaporanRugiLabaProdukSTS(DateTime awal, DateTime akhir)
        {
            return await _featRepository.getLaporanRugiLabaProdukSTS(awal, akhir);
        }

        public async Task<LaporanPembayaranPiutangDetailResponse> getLaporanPembayaranPiutangDetail(DateTime awal, DateTime akhir)
        {
            return await _featRepository.getLaporanPembayaranPiutangDetail(awal, akhir);
        }
    }
}