﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ampera.Entity
{
    public class UserPermission
    {
        [Key]
        public Guid userPermissionId { get; set; }
        public Guid userId { get; set; }
        public Guid permissionId { get; set; }
        public bool isAuthorized { get; set; }
    }
}
