﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ampera.Entity
{
    public class ModalTambahan
    {
        [Key]
        public string kodeBarang { get; set; }
        public decimal nilaiTambahan { get; set; }
        public DateTime updatedDate { get; set; }
    }
}
