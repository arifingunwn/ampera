﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ampera.Entity
{
    public class Permissions
    {
        public const string DaftarBarangView = "0b28a8a2-a67d-4e26-92e9-59e6fdc63383";
        public const string SalesDailyReport = "10ccae21-72bf-4d2d-a559-ef64bb523cb5";
        public const string OmzetBarang = "b9ecf240-796d-45fd-a96e-0acab36ea098";
        public const string OmzetCustomer = "477feadc-a95b-4aaa-b5d4-ae806d9cf84b";
        public const string ModalTambahan = "f5d8a7b5-d36c-4dd7-af50-6b3f70dde9f3";
        public const string User = "9b308860-054a-441d-a5e5-bf6fcaeb4cac";
    }
}
