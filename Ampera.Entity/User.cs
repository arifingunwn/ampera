﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ampera.Entity
{
    public class User
    {
        [Key]
        public Guid userId { get; set; }
        public string username { get; set; }
        public string password { get; set; }
    }
}
