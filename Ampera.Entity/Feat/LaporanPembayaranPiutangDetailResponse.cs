﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ampera.Entity.Feat
{
    public class LaporanPembayaranPiutangDetailResponse
    {
        public string result { get; set; }
        public List<LaporanPembayaranPiutangDetail> data { get; set; }
    }

    public class LaporanPembayaranPiutangDetail
    {
        [JsonProperty("NO BUKTI")]
        public string NOBUKTI { get; set; }

        [JsonProperty("KODE REKAN")]
        public string KODEREKAN { get; set; }

        [JsonProperty("NAMA REKAN")]
        public string NAMAREKAN { get; set; }
        public string ALAMAT { get; set; }
        public string KOTA { get; set; }

        [JsonProperty("KODE POS")]
        public string KODEPOS { get; set; }
        public string NEGARA { get; set; }
        public string NPWP { get; set; }
        public string TEXT1 { get; set; }
        public string TEXT2 { get; set; }
        public string TEXT3 { get; set; }
        public string TEXT4 { get; set; }
        public string TEXT5 { get; set; }
        public string NUMERIC1 { get; set; }
        public string NUMERIC2 { get; set; }
        public string NUMERIC3 { get; set; }
        public string NUMERIC4 { get; set; }
        public string NUMERIC5 { get; set; }
        public DateTime TANGGAL1 { get; set; }
        public DateTime TANGGAL2 { get; set; }
        public DateTime TANGGAL3 { get; set; }
        public DateTime TANGGAL4 { get; set; }
        public DateTime TANGGAL5 { get; set; }
        public string KURS { get; set; }

        [JsonProperty("AKUN BAYAR")]
        public string AKUNBAYAR { get; set; }

        [JsonProperty("NAMA PERKIRAAN BAYAR")]
        public string NAMAPERKIRAANBAYAR { get; set; }

        [JsonProperty("TANGGAL BAYAR")]
        public DateTime TANGGALBAYAR { get; set; }

        [JsonProperty("TANGGAL TERIMA")]
        public DateTime TANGGALTERIMA { get; set; }

        [JsonProperty("NO CEK")]
        public string NOCEK { get; set; }
        public string KETERANGAN { get; set; }

        [JsonProperty("NO FAKTUR")]
        public string NOFAKTUR { get; set; }

        [JsonProperty("TANGGAL FAKTUR")]
        public DateTime TANGGALFAKTUR { get; set; }

        [JsonProperty("TOTAL PENJUALAN")]
        public decimal TOTALPENJUALAN { get; set; }

        [JsonProperty("NO RETUR")]
        public string NORETUR { get; set; }

        [JsonProperty("TOTAL RETUR")]
        public decimal TOTALRETUR { get; set; }

        [JsonProperty("DETAIL KETERANGAN")]
        public string DETAILKETERANGAN { get; set; }

        [JsonProperty("NO TAGIHAN")]
        public string NOTAGIHAN { get; set; }

        [JsonProperty("TOTAL TAGIHAN")]
        public decimal TOTALTAGIHAN { get; set; }
        public decimal BAYAR { get; set; }
        public decimal PIUTANG { get; set; }

        [JsonProperty("AKUN POTONGAN")]
        public string AKUNPOTONGAN { get; set; }

        [JsonProperty("NAMA PERKIRAAN POTONGAN")]
        public string NAMAPERKIRAANPOTONGAN { get; set; }
        public string POTONGAN { get; set; }

        [JsonProperty("NO PROYEK")]
        public string NOPROYEK { get; set; }

        [JsonProperty("NAMA PROYEK")]
        public string NAMAPROYEK { get; set; }
    }
}
