﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ampera.Entity.Feat
{
    public class LaporanRugiLabaProdukSTSReponse
    {
        public string result { get; set; }
        public IEnumerable<LaporanRugiLabaProdukSTS> data { get; set; } = new List<LaporanRugiLabaProdukSTS>();
    }

    public class LaporanRugiLabaProdukSTS
    {
        [JsonProperty("NO FAKTUR")]
        public string NOFAKTUR { get; set; }

        [JsonProperty("TANGGAL FAKTUR")]
        public string TANGGALFAKTUR { get; set; }

        [JsonProperty("ID PELANGGAN")]
        public string IDPELANGGAN { get; set; }

        [JsonProperty("KODE REKAN")]
        public string KODEREKAN { get; set; }

        [JsonProperty("NAMA REKAN")]
        public string NAMAREKAN { get; set; }
        public string ALAMAT { get; set; }
        public string KOTA { get; set; }

        [JsonProperty("KODE POS")]
        public string KODEPOS { get; set; }
        public string NEGARA { get; set; }

        [JsonProperty("ID SALES")]
        public string IDSALES { get; set; }

        [JsonProperty("KODE SALESMAN")]
        public string KODESALESMAN { get; set; }

        [JsonProperty("NAMA SALESMAN")]
        public string NAMASALESMAN { get; set; }

        [JsonProperty("NAMA TERMIN")]
        public string NAMATERMIN { get; set; }
        public string KURS { get; set; }

        [JsonProperty("NO PPN")]
        public string NOPPN { get; set; }

        [JsonProperty("TANGGAL PPN")]
        public string TANGGALPPN { get; set; }
        public string PENJUALAN { get; set; }
        public string DISKON { get; set; }

        [JsonProperty("HARGA DISKON")]
        public string HARGADISKON { get; set; }

        [JsonProperty("JUMLAH DISKON")]
        public string JUMLAHDISKON { get; set; }
        public string NETTO { get; set; }
        public string PAJAK { get; set; }

        [JsonProperty("TOTAL PENJUALAN")]
        public string TOTALPENJUALAN { get; set; }

        [JsonProperty("TOTAL TRANSAKSI")]
        public string TOTALTRANSAKSI { get; set; }
        public string PIUTANG { get; set; }
        public string KETERANGAN { get; set; }
        public string SEQ { get; set; }

        [JsonProperty("KODE BARANG")]
        public string KODEBARANG { get; set; }

        [JsonProperty("NAMA BARANG")]
        public string NAMABARANG { get; set; }
        public string DESKRIPSI { get; set; }
        public string UNIT { get; set; }
        public string SATUAN { get; set; }
        public string RASIO { get; set; }
        public decimal JUMLAH { get; set; }

        [JsonProperty("JUMLAH RASIO")]
        public string JUMLAHRASIO { get; set; }
        public string JUMLAH1 { get; set; }
        public string JUMLAH2 { get; set; }
        public string JUMLAH3 { get; set; }
        public string UNIT1 { get; set; }
        public string UNIT2 { get; set; }
        public string UNIT3 { get; set; }
        public string HARGA { get; set; }

        [JsonProperty("DISKON DETAIL")]
        public string DISKONDETAIL { get; set; }

        [JsonProperty("HARGA DISKON DETAIL")]
        public string HARGADISKONDETAIL { get; set; }

        [JsonProperty("DISKON GLOBAL")]
        public string DISKONGLOBAL { get; set; }

        [JsonProperty("JUMLAH DISKON DETAIL")]
        public string JUMLAHDISKONDETAIL { get; set; }

        [JsonProperty("KODE PAJAK")]
        public string KODEPAJAK { get; set; }
        public string TARIF { get; set; }

        [JsonProperty("JUMLAH HARGA")]
        public string JUMLAHHARGA { get; set; }
        public string HPP { get; set; }

        [JsonProperty("PAJAK DETAIL")]
        public string PAJAKDETAIL { get; set; }

        [JsonProperty("TOTAL HARGA")]
        public decimal TOTALHARGA { get; set; }

        [JsonProperty("JUMLAH HARGA BERSIH")]
        public string JUMLAHHARGABERSIH { get; set; }
        public decimal LABA { get; set; }
        public string PROYEK { get; set; }

        [JsonProperty("KETERANGAN DETAIL")]
        public string KETERANGANDETAIL { get; set; }

        [JsonProperty("TEXT1 DETAIL")]
        public string TEXT1DETAIL { get; set; }

        [JsonProperty("TEXT2 DETAIL")]
        public string TEXT2DETAIL { get; set; }

        [JsonProperty("TEXT3 DETAIL")]
        public string TEXT3DETAIL { get; set; }

        [JsonProperty("TEXT4 DETAIL")]
        public string TEXT4DETAIL { get; set; }

        [JsonProperty("TEXT5 DETAIL")]
        public string TEXT5DETAIL { get; set; }

        [JsonProperty("NUMERIC1 DETAIL")]
        public string NUMERIC1DETAIL { get; set; }

        [JsonProperty("NUMERIC2 DETAIL")]
        public string NUMERIC2DETAIL { get; set; }

        [JsonProperty("NUMERIC3 DETAIL")]
        public string NUMERIC3DETAIL { get; set; }

        [JsonProperty("NUMERIC4 DETAIL")]
        public string NUMERIC4DETAIL { get; set; }

        [JsonProperty("NUMERIC5 DETAIL")]
        public string NUMERIC5DETAIL { get; set; }

        [JsonProperty("TANGGAL1 DETAIL")]
        public string TANGGAL1DETAIL { get; set; }

        [JsonProperty("TANGGAL2 DETAIL")]
        public string TANGGAL2DETAIL { get; set; }

        [JsonProperty("TANGGAL3 DETAIL")]
        public string TANGGAL3DETAIL { get; set; }

        [JsonProperty("TANGGAL4 DETAIL")]
        public string TANGGAL4DETAIL { get; set; }

        [JsonProperty("TANGGAL5 DETAIL")]
        public string TANGGAL5DETAIL { get; set; }
        public string TIPE { get; set; }
        public string POS { get; set; }
    }
}
