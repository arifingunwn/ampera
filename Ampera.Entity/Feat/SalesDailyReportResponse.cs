﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ampera.Entity.Feat
{
    public class SalesDailyReportResponse
    {
        public string result { get; set; }
        public List<SalesDailyReport> data { get; set; }
    }

    public class SalesDailyReport
    {
        [JsonProperty(PropertyName = "KURS")]
        public string KURS;

        [JsonProperty(PropertyName = "KODE REKAN")]
        public string KODEREKAN;

        [JsonProperty(PropertyName = "NAMA REKAN")]
        public string NAMAREKAN;

        [JsonProperty(PropertyName = "TEXT1")]
        public string TEXT1;

        [JsonProperty(PropertyName = "TEXT2")]
        public string TEXT2;

        [JsonProperty(PropertyName = "TEXT3")]
        public string TEXT3;

        [JsonProperty(PropertyName = "TEXT4")]
        public string TEXT4;

        [JsonProperty(PropertyName = "TEXT5")]
        public string TEXT5;

        [JsonProperty(PropertyName = "NUMERIC1")]
        public string NUMERIC1;

        [JsonProperty(PropertyName = "NUMERIC2")]
        public string NUMERIC2;

        [JsonProperty(PropertyName = "NUMERIC3")]
        public string NUMERIC3;

        [JsonProperty(PropertyName = "NUMERIC4")]
        public string NUMERIC4;

        [JsonProperty(PropertyName = "NUMERIC5")]
        public string NUMERIC5;

        [JsonProperty(PropertyName = "TANGGAL1")]
        public string TANGGAL1;

        [JsonProperty(PropertyName = "TANGGAL2")]
        public string TANGGAL2;

        [JsonProperty(PropertyName = "TANGGAL3")]
        public string TANGGAL3;

        [JsonProperty(PropertyName = "TANGGAL4")]
        public string TANGGAL4;

        [JsonProperty(PropertyName = "TANGGAL5")]
        public string TANGGAL5;

        [JsonProperty(PropertyName = "KODE SALESMAN")]
        public string KODESALESMAN;

        [JsonProperty(PropertyName = "NAMA SALESMAN")]
        public string NAMASALESMAN;

        [JsonProperty(PropertyName = "TARGET")]
        public string TARGET;

        [JsonProperty(PropertyName = "KOMISI")]
        public string KOMISI;

        [JsonProperty(PropertyName = "TELEPON")]
        public string TELEPON;

        [JsonProperty(PropertyName = "KODE BARANG")]
        public string KODEBARANG;

        [JsonProperty(PropertyName = "NAMA BARANG")]
        public string NAMABARANG;

        [JsonProperty(PropertyName = "KODE INDUK")]
        public string KODEINDUK;

        [JsonProperty(PropertyName = "NAMA INDUK")]
        public string NAMAINDUK;

        [JsonProperty(PropertyName = "JUMLAH")]
        public string JUMLAH;

        [JsonProperty(PropertyName = "UNIT1")]
        public string UNIT1;

        [JsonProperty(PropertyName = "UNIT2")]
        public string UNIT2;

        [JsonProperty(PropertyName = "RASIO2")]
        public string RASIO2;

        [JsonProperty(PropertyName = "UNIT3")]
        public string UNIT3;

        [JsonProperty(PropertyName = "RASIO3")]
        public string RASIO3;

        [JsonProperty(PropertyName = "JUMLAH HARGA KURS")]
        public string JUMLAHHARGAKURS;

        [JsonProperty(PropertyName = "DISKON KURS")]
        public string DISKONKURS;

        [JsonProperty(PropertyName = "JUMLAH HARGA BERSIH KURS")]
        public string JUMLAHHARGABERSIHKURS;

        [JsonProperty(PropertyName = "RETUR")]
        public decimal RETUR;

        [JsonProperty(PropertyName = "JUAL")]
        public string JUAL;

        [JsonProperty(PropertyName = "DISKON")]
        public string DISKON;

        [JsonProperty(PropertyName = "JUMLAH HARGA BERSIH")]
        public decimal JUMLAHHARGABERSIH;

        [JsonProperty(PropertyName = "TEXT1 DETAIL")]
        public string TEXT1DETAIL;

        [JsonProperty(PropertyName = "TEXT2 DETAIL")]
        public string TEXT2DETAIL;

        [JsonProperty(PropertyName = "TEXT3 DETAIL")]
        public string TEXT3DETAIL;

        [JsonProperty(PropertyName = "TEXT4 DETAIL")]
        public string TEXT4DETAIL;

        [JsonProperty(PropertyName = "TEXT5 DETAIL")]
        public string TEXT5DETAIL;

        [JsonProperty(PropertyName = "NUMERIC1 DETAIL")]
        public string NUMERIC1DETAIL;

        [JsonProperty(PropertyName = "NUMERIC2 DETAIL")]
        public string NUMERIC2DETAIL;

        [JsonProperty(PropertyName = "NUMERIC3 DETAIL")]
        public string NUMERIC3DETAIL;

        [JsonProperty(PropertyName = "NUMERIC4 DETAIL")]
        public string NUMERIC4DETAIL;

        [JsonProperty(PropertyName = "NUMERIC5 DETAIL")]
        public string NUMERIC5DETAIL;

        [JsonProperty(PropertyName = "TANGGAL1 DETAIL")]
        public string TANGGAL1DETAIL;

        [JsonProperty(PropertyName = "TANGGAL2 DETAIL")]
        public string TANGGAL2DETAIL;

        [JsonProperty(PropertyName = "TANGGAL3 DETAIL")]
        public string TANGGAL3DETAIL;

        [JsonProperty(PropertyName = "TANGGAL4 DETAIL")]
        public string TANGGAL4DETAIL;

        [JsonProperty(PropertyName = "TANGGAL5 DETAIL")]
        public string TANGGAL5DETAIL;
    }
}
