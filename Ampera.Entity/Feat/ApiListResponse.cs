﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Ampera.Entity.Feat
{
    public class ApiListResponse
    {
        public string result { get; set; }
        public List<ApiList> data { get; set; }
    }

    public class ApiList
    {
        public int id { get; set; }
        public string nama { get; set; }   
        public int tipe { get; set; }
        public string keterangan { get; set; }

        [JsonProperty(PropertyName = "tipe filter")]
        public string tipeFilter { get; set; }

        [JsonProperty(PropertyName = "judul filter1")]
        public string judulFilter1 { get; set; }

        [JsonProperty(PropertyName = "judul filter2")]
        public string judulFilter2 { get; set; }
    }
}
