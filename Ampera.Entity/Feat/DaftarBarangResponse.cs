﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ampera.Entity.Feat
{
    public class DaftarBarangResponse
    {
        public string result { get; set; }
        public List<DaftarBarang> data { get; set; }
    }

    public class DaftarBarang
    {
        public DaftarBarang(string kodeBarang, string namaBarang)
        {
            this.kodeBarang = kodeBarang;
            this.namaBarang = namaBarang;
        }

        [JsonProperty(PropertyName = "KODE BARANG")]
        public string kodeBarang { get; set; }

        [JsonProperty(PropertyName = "NAMA BARANG")]
        public string namaBarang { get; set; }

        [JsonProperty(PropertyName = "KODE INDUK")]
        public string kodeInduk { get; set; }

        [JsonProperty(PropertyName = "NAMA INDUK")]
        public string namaInduk { get; set; }

        public string unit { get; set; }
        public string unit2 { get; set; }
        public string rasio2 { get; set; }
        public string unit3 { get; set; }
        public string rasio3 { get; set; }
        public string text1 { get; set; }
        public string text2 { get; set; }
        public string text3 { get; set; }
        public string text4 { get; set; }
        public string text5 { get; set; }
        public string numeric1 { get; set; }
        public string numeric2 { get; set; }
        public string numeric3 { get; set; }
        public string numeric4 { get; set; }
        public string numeric5 { get; set; }
        public string tanggal1 { get; set; }
        public string tanggal2 { get; set; }
        public string tanggal3 { get; set; }
        public string tanggal4 { get; set; }
        public string tanggal5 { get; set; }
        public string tipe { get; set; }
        [JsonProperty(PropertyName = "TIDAK AKTIF")]
        public string tidakAktif { get; set; }
        [JsonProperty(PropertyName = "AKUN PERSEDIAAN")]
        public string akunPersediaan { get; set; }
        [JsonProperty(PropertyName = "NAMA PERKIRAAN")]
        public string namaPerkiraan { get; set; }
        public string jumlah{ get; set; }

    }
}
