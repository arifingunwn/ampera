﻿using Ampera.Entity.Feat;
using Ampera.Models;
using AutoMapper;

namespace Ampera.Profiles
{
    public class AmperaProfile : Profile
    {
        public AmperaProfile()
        {
            CreateMap<DaftarBarang, DaftarBarangModel>().ForMember(x => x.satuan, y => y.MapFrom(z => z.unit));
            CreateMap<Ampera.Entity.User, UserListViewItemModel>();
            CreateMap<Ampera.Entity.Feat.LaporanPembayaranPiutangDetail, OnTimePaymentIndexViewItemModel>();
        }
    }
}
