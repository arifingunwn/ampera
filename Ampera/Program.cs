using Ampera;
using Ampera.Authorization;
using Ampera.Repositories;
using Ampera.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddDbContext<AmperaDBContext>();
builder.Services.AddControllersWithViews();
builder.Services.AddHttpClient();
builder.Configuration.AddJsonFile("appsettings.json");
builder.Services.Configure<AuthorizationSettings>(builder.Configuration.GetSection("AuthorizationSettings"));
builder.Services.Configure<FeatAPISettings>(builder.Configuration.GetSection("FeatAPISettings"));
builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
builder.Services.AddMemoryCache();
builder.Services.AddScoped<IAuthorizationService, AuthorizationService>();
builder.Services.AddScoped<IFeatRepository, FeatRepositoryImpl>();
builder.Services.AddScoped<IFeatCachedRepository, FeatCachedRepositoryImpl>();
builder.Services.AddScoped<IFeatService, FeatServiceImpl>();
builder.Services.AddSession(options =>
{
    options.IdleTimeout = TimeSpan.FromDays(1);
    options.Cookie.HttpOnly = true;
    options.Cookie.IsEssential = true;
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
}
app.UseStaticFiles();

app.UseRouting();
app.UseSession();
app.UseMiddleware<JwtMiddleware>();
app.UseAuthentication();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Main}/{action=Index}/{id?}");

app.Run();
