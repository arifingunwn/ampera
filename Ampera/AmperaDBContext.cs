﻿using Ampera.Entity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ampera
{
    public class AmperaDBContext : DbContext
    {
        public DbSet<ModalTambahan> ModalTambahan { get; set; }
        public DbSet<User> User { get; set; }
        public DbSet<UserPermission> UserPermission { get; set; }
        public string DbPath { get; }
        public AmperaDBContext()
        {
            string folderPath = Directory.GetCurrentDirectory();
            DbPath = System.IO.Path.Join(folderPath, "Database", "ampera.db");
        }

        protected override void OnConfiguring(DbContextOptionsBuilder options) => options.UseSqlite($"Data Source={DbPath}");
    }
}
