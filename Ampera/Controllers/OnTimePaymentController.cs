﻿using Ampera.Models;
using Ampera.Services;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;

namespace Ampera.Controllers
{
    public class OnTimePaymentController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IFeatService _featService;

        public OnTimePaymentController(IFeatService featService, IMapper mapper)
        {
            _featService = featService;
            _mapper = mapper;
        }

        public IActionResult Index()
        {
            OnTimePaymentIndexViewModel model = new OnTimePaymentIndexViewModel();
            model.awal = DateTime.Now;
            model.akhir = DateTime.Now;
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> IndexAsync(DateTime awal, DateTime akhir)
        {
            var response = await _featService.getLaporanPembayaranPiutangDetail(awal, akhir);

            OnTimePaymentIndexViewModel model = new OnTimePaymentIndexViewModel();
            model.awal = awal;
            model.akhir = akhir;
            model.Items = _mapper.Map<List<OnTimePaymentIndexViewItemModel>>(response.data);
            
            return View(model);
        }
    }
}
