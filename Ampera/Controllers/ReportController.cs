﻿using Ampera.Entity.Feat;
using Ampera.Models;
using Ampera.Services;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace Ampera.Controllers
{
    public class ReportController : Controller
    {
        private AmperaDBContext _dbContext;
        private readonly IFeatService _featService;

        public ReportController(AmperaDBContext dBContext, IFeatService featService)
        {
            _dbContext = dBContext;
            _featService = featService;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult SalesDailyReportView()
        {
            return View(new SalesDailyReportModel()
            {
                awal = DateTime.Parse(DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString() + "-01"),
                akhir = DateTime.Parse(DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month).ToString()),
            });
        }

        [HttpPost]
        public async Task<IActionResult> GetSalesDailyReport(AwalAkhirRequestParameter param)
        {
            SalesDailyReportModel model = new SalesDailyReportModel();
            model.awal = DateTime.Parse(param.awal);
            model.akhir = DateTime.Parse(param.akhir);
            SalesDailyReportResponse featData = await _featService.getSalesDailyReport(model.awal, model.akhir);

            List<SalesDailyReportItemModel> result = new List<SalesDailyReportItemModel>();
            var multiDictionary = _getSalesDailyReportDictionary(featData.data);
            foreach (var item in featData.data)
            {
                if (item.TEXT2DETAIL.Equals("OWNER", StringComparison.OrdinalIgnoreCase)) {
                    multiDictionary[item.KODESALESMAN][item.KODEREKAN].sumOfOwner += item.JUMLAHHARGABERSIH;
                }
                else if (item.TEXT2DETAIL.Equals("YOZURI", StringComparison.OrdinalIgnoreCase)) {
                    multiDictionary[item.KODESALESMAN][item.KODEREKAN].sumOfYozuri += item.JUMLAHHARGABERSIH;
                }
                else if (item.TEXT2DETAIL.Equals("YOZURI", StringComparison.OrdinalIgnoreCase)) {
                    multiDictionary[item.KODESALESMAN][item.KODEREKAN].sumOfYZR += item.JUMLAHHARGABERSIH;
                }
                else if (item.TEXT2DETAIL.Equals("YOZURI", StringComparison.OrdinalIgnoreCase)) {
                    multiDictionary[item.KODESALESMAN][item.KODEREKAN].sumOfSmartChoice += item.JUMLAHHARGABERSIH;
                }
                else {
                    multiDictionary[item.KODESALESMAN][item.KODEREKAN].sumOfOther += item.JUMLAHHARGABERSIH;
                }

                multiDictionary[item.KODESALESMAN][item.KODEREKAN].sumOfRetur += item.RETUR;
                multiDictionary[item.KODESALESMAN][item.KODEREKAN].sumOfOmzet += item.JUMLAHHARGABERSIH;
                multiDictionary[item.KODESALESMAN][item.KODEREKAN].sumOfTotal += item.JUMLAHHARGABERSIH;
                multiDictionary[item.KODESALESMAN][item.KODEREKAN].namaRekan = item.NAMAREKAN;
                multiDictionary[item.KODESALESMAN][item.KODEREKAN].namaSales = item.NAMASALESMAN;

                model.DropDownListFilterSales.Add(item.NAMASALESMAN);
                model.DropDownListFilterRekan.Add(item.NAMAREKAN);
            }

            List<SalesDailyReportSegmentModel> segments = new List<SalesDailyReportSegmentModel>();
            foreach (var sales in multiDictionary)
            {
                SalesDailyReportSegmentModel segment = new SalesDailyReportSegmentModel();
                foreach (var rekan in multiDictionary[sales.Key])
                {
                    segment.items.Add(new SalesDailyReportItemModel()
                    {
                        kodeSales = rekan.Value.kodeSales,
                        namaSales = rekan.Value.namaSales,
                        kodeRekan = rekan.Value.kodeRekan,
                        namaRekan = rekan.Value.namaRekan,
                        sumOfOwner = rekan.Value.sumOfOwner,
                        sumOfYozuri = rekan.Value.sumOfYozuri,
                        sumOfYZR = rekan.Value.sumOfYZR,
                        sumOfSmartChoice = rekan.Value.sumOfSmartChoice,
                        sumOfOther = rekan.Value.sumOfOther,
                        sumOfRetur = rekan.Value.sumOfRetur,
                        sumOfOmzet = rekan.Value.sumOfOmzet,
                        sumOfTotal = rekan.Value.sumOfTotal
                    });

                    segment.subTotalOfOwner += rekan.Value.sumOfOwner;
                    segment.subTotalOfYozuri += rekan.Value.sumOfYozuri;
                    segment.subTotalOfYZR += rekan.Value.sumOfYZR;
                    segment.subTotalOfSmartChoice += rekan.Value.sumOfSmartChoice;
                    segment.subTotalOfOther += rekan.Value.sumOfOther;
                    segment.subTotalOfRetur += rekan.Value.sumOfRetur;
                    segment.subTotalOfOmzet += rekan.Value.sumOfOmzet;
                    segment.subTotal += rekan.Value.sumOfTotal;
                }

                segment.items = segment.items.OrderByDescending(x => x.sumOfTotal).ToList();

                model.grandTotalOfOwner += segment.subTotalOfOwner;
                model.grandTotalOfYozuri += segment.subTotalOfYozuri;
                model.grandTotalOfYZR += segment.subTotalOfYZR;
                model.grandTotalOfSmartChoice += segment.subTotalOfSmartChoice;
                model.grandTotalOfRetur += segment.subTotalOfRetur;
                model.grandTotalOfOther += segment.subTotalOfOther;
                model.grandTotalOfOmzet += segment.subTotalOfOmzet;
                model.grandTotal += segment.subTotal;
                segments.Add(segment);
            }

            model.segments = segments.OrderByDescending(x => x.subTotal).ToList();
            model.DropDownListFilterSales = model.DropDownListFilterSales.OrderBy(x => x).ToHashSet();
            model.DropDownListFilterRekan = model.DropDownListFilterRekan.OrderBy(x => x).ToHashSet();

            return View("SalesDailyReportView", model);
        }

        private Dictionary<string, Dictionary<string, SalesDailyReportItemModel>> _getSalesDailyReportDictionary(List<SalesDailyReport> data)
        {
            var multiDictionary = new Dictionary<string, Dictionary<string, SalesDailyReportItemModel>>();
            data.GroupBy(x => new { x.KODESALESMAN, x.KODEREKAN }).Select(x => new
            {
                kodeSales = x.Key.KODESALESMAN,
                kodeRekan = x.Key.KODEREKAN

            }).ToList().ForEach(x =>
            {
                if (!multiDictionary.ContainsKey(x.kodeSales))
                {
                    multiDictionary.Add(x.kodeSales, new Dictionary<string, SalesDailyReportItemModel>());
                }
                else
                {
                    if (!multiDictionary[x.kodeSales].ContainsKey(x.kodeRekan))
                    {
                        multiDictionary[x.kodeSales].Add(x.kodeRekan, new SalesDailyReportItemModel());
                    }
                }

                multiDictionary[x.kodeSales][x.kodeRekan] = new SalesDailyReportItemModel()
                {
                    kodeSales = x.kodeSales,
                    kodeRekan = x.kodeRekan
                };
            });

            return multiDictionary;
        }

        public IActionResult OmzetCustomerView()
        {
            return View(new OmzetCustomerViewModel()
            {
                awal = DateTime.Parse(DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString() + "-01"),
                akhir = DateTime.Parse(DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month).ToString()),
                rowCount = 10,
                sortOrder = "DESC"
            });
        }

        [HttpPost]
        public async Task<IActionResult> OmzetCustomerView(OmzetCustomerRequest param)
        {
            var featResponse = await _featService.getSalesDailyReport(param.awal, param.akhir);
            OmzetCustomerViewModel model = new OmzetCustomerViewModel();
            model.awal = param.awal;
            model.akhir = param.akhir;
            model.sortOrder = param.sortOrder;
            model.rowCount = param.rowCount;

            var groupedItems = featResponse.data.GroupBy(x => new { x.KODEREKAN, x.KODESALESMAN, x.NAMAREKAN, x.NAMASALESMAN }).Select(x => new OmzetCustomerItemModel()
            {
                kodeRekan = x.Key.KODEREKAN,
                kodeSales = x.Key.KODESALESMAN,
                namaRekan = x.Key.NAMAREKAN,
                namaSales = x.Key.NAMASALESMAN,
                totalOmzet = x.Sum(y => y.JUMLAHHARGABERSIH)
            });

            if(model.sortOrder == "ASC")
            {
                groupedItems = groupedItems.OrderBy(x => x.totalOmzet);
            } else
            {
                groupedItems = groupedItems.OrderByDescending(x => x.totalOmzet);
            }

            model.Items = groupedItems.Take(param.rowCount == null ? int.MaxValue : param.rowCount);

            return View(model);
        }

        public IActionResult OmzetBarangView()
        {
            return View(new OmzetBarangViewModel()
            {
                awal = DateTime.Parse(DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString() + "-01"),
                akhir = DateTime.Parse(DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month).ToString()),
                namaBarang = string.Empty,
                rowCount = 10,
                sortBy = "LABA",
                sortOrder = "DESC"
            });
        }

        [HttpPost]
        public async Task<IActionResult> OmzetBarangView(OmzetBarangRequest param)
        {
            OmzetBarangViewModel model = new OmzetBarangViewModel();
            
            var featResponse = await _featService.getLaporanRugiLabaProdukSTS(param.awal, param.akhir);

            var dictModalTambahan = _dbContext.ModalTambahan.ToDictionary(x => x.kodeBarang, y => y.nilaiTambahan);

            model.awal = param.awal;
            model.akhir = param.akhir;
            model.namaBarang = param.namaBarang;
            model.sortOrder = param.sortOrder;
            model.sortBy = param.sortBy;
            model.rowCount = param.rowCount;

            var filteredData = featResponse.data.Where(x => (param.namaBarang == null) || (x.NAMABARANG.Contains(param.namaBarang, StringComparison.CurrentCultureIgnoreCase)));

            var groupedData = filteredData.GroupBy(x => new { x.KODEBARANG, x.NAMABARANG, x.SATUAN }).Select(x => new OmzetBarangItemModel
            {
                kodeBarang = x.Key.KODEBARANG,
                namaBarang = x.Key.NAMABARANG,
                qty = x.Sum(y => y.JUMLAH),
                omzet = x.Sum(y => y.TOTALHARGA),
                labaAwal = x.Sum(y => y.LABA),
                labalAkhir = x.Sum(y => y.LABA),
                modalTambahan = 0,
                satuan = x.Key.SATUAN
            }).ToList();

            foreach (var item in groupedData)
            {
                if (dictModalTambahan.ContainsKey(item.kodeBarang))
                {
                    item.modalTambahan = dictModalTambahan[item.kodeBarang];
                    item.labalAkhir = item.labaAwal - (item.qty * item.modalTambahan);
                }
            }

            if (model.sortOrder == "ASC")
            {
                if (model.sortBy == "LABA")
                {
                    groupedData = groupedData.OrderBy(x => x.labalAkhir).ToList();
                }
                else if (model.sortBy == "OMZET")
                {
                    groupedData = groupedData.OrderBy(x => x.omzet).ToList();
                }
                else if (model.sortBy == "QUANTITY")
                {
                    groupedData = groupedData.OrderBy(x => x.qty).ToList();
                }
            }
            else
            {
                if (model.sortBy == "LABA")
                {
                    groupedData = groupedData.OrderByDescending(x => x.labalAkhir).ToList();
                }
                else if (model.sortBy == "OMZET")
                {
                    groupedData = groupedData.OrderByDescending(x => x.omzet).ToList();
                }
                else if (model.sortBy == "QUANTITY")
                {
                    groupedData = groupedData.OrderByDescending(x => x.qty).ToList();
                }
            }

            model.Items = groupedData.Take(model.rowCount == null ? int.MaxValue : model.rowCount).ToList();

            return View(model);
        }
    }
}
