﻿using Microsoft.AspNetCore.Mvc;
using Ampera.Services;
using Ampera.Entity.Feat;
using AutoMapper;
using Ampera.Models;
using Ampera.Entity;
using System.Collections;

namespace Ampera.Controllers
{
    public class DaftarBarangController : Controller
    {
        private readonly IFeatService _featService;
        private readonly IMapper _mapper;
        private readonly AmperaDBContext db;
        public DaftarBarangController(AmperaDBContext amperaDBContext, IFeatService featService, IMapper mapper)
        {
            db = amperaDBContext;
            _featService = featService;
            _mapper = mapper;
        }
        public async Task<IActionResult> Index()
        {
            var daftarBarangResponse = await _featService.getDaftarBarang(DateTime.Now.AddDays(1));
            DaftarBarangListModel model = new DaftarBarangListModel();
            model.dataBarangList = _mapper.Map<List<DaftarBarangModel>>(daftarBarangResponse.data);

            Dictionary<string, DaftarBarangModel> dictDaftarBarang = model.dataBarangList.ToDictionary(keySelector: x => x.kodeBarang, elementSelector: x => x);
            db.ModalTambahan.ToList().ForEach(x =>
            {
                if(dictDaftarBarang.ContainsKey(x.kodeBarang)) dictDaftarBarang[x.kodeBarang].nilaiTambahan = x.nilaiTambahan;
            });

            model.dataBarangList = dictDaftarBarang.Values.ToList();

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> CreateOrUpdate(Models.CreateOrUpdateModalTambahanModel request)
        {
            if (request == null) return BadRequest("Null params");
            if (request.kodeBarang == null) return BadRequest("Kode Barang harus diisi");
            if (request.nilaiTambahan == null) return BadRequest("Nilai Tambahan harus diisi");

            var modalTambahan = db.ModalTambahan.Find(request.kodeBarang);
            if(modalTambahan == null)
            {
                db.Add(new ModalTambahan
                {
                    kodeBarang = request.kodeBarang,
                    nilaiTambahan = request.nilaiTambahan,
                    updatedDate = DateTime.Now
                });
                db.SaveChanges();
            } else
            {
                modalTambahan.nilaiTambahan = request.nilaiTambahan;
                modalTambahan.updatedDate = DateTime.Now;
                db.SaveChanges();
            }

            return Ok();
        }
    }
}
