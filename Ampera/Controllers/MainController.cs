﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using RestSharp;
using Ampera.Services;
using Ampera.Entity.Feat;
using Ampera.Models;
using AutoMapper;
using Ampera.Authorization;
using Ampera.Repositories;
using Microsoft.Extensions.Options;

namespace Ampera.Controllers
{
    [Authorization]
    public class MainController : Controller
    {
        private readonly IFeatService _featService;
        private readonly FeatAPISettings _featAPISettings;
        public MainController(IFeatService featService, IOptions<FeatAPISettings> featApiSettings)
        {
            _featService = featService;
            _featAPISettings = featApiSettings.Value;
        }
        public async Task<IActionResult> Index()
        {
            HomePageModel model = new HomePageModel();

            var result = await _featService.getApiList();
            if(result == null)
            {
                model.FeatApiStatusOK = false;
            } else
            {
                model.FeatApiStatusOK = true;
            }

            model.homePageApiIdListItems.Add(new HomePageApiIdListItem() { ApiId = _featAPISettings.SalesDailyReportApiId, ApiName = "Sales Daily Report" });
            model.homePageApiIdListItems.Add(new HomePageApiIdListItem() { ApiId = _featAPISettings.DaftarBarangApiId, ApiName = "Daftar Barang" });
            model.homePageApiIdListItems.Add(new HomePageApiIdListItem() { ApiId = _featAPISettings.LaporanRugiLabaProdukSTSApiId, ApiName = "Laporan Rugi Laba Produk STS" });
            return View(model);
        }
    }
}
