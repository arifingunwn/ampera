﻿using Ampera.Authorization;
using Ampera.Entity;
using Ampera.Models;
using Ampera.Services;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using BCryptNet = BCrypt.Net.BCrypt;
        
namespace Ampera.Controllers
{
    public class UserController : Controller
    {
        private readonly AmperaDBContext _dBContext;
        private readonly IMapper _mapper;
        private readonly IAuthorizationService _authorizationService;
        public UserController(AmperaDBContext dbContext, IMapper mapper, IAuthorizationService authorizationService)
        {
            this._dBContext = dbContext;
            _mapper = mapper;
            _authorizationService = authorizationService;
        }

        public IActionResult Index()
        {
            return View(new UserListViewModel()
            {
                users = _mapper.Map<List<UserListViewItemModel>>(_dBContext.User.ToList())
            });
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(CreateUserModel model)
        {
            string username = model.username.Trim();
            if(_dBContext.User.Where(x => x.username == username).Count() > 0)
            {
                TempData["errorMessage"] = "Username [" + username + "] sudah ada.";
                return View(model);
            }

            var hashPassword = BCryptNet.HashPassword(model.password);

            _dBContext.User.Add(new Entity.User
            {
                userId = Guid.NewGuid(),
                username = username,
                password = hashPassword
            });

            _dBContext.SaveChanges();

            TempData["infoMessage"] = "Sukses tambah User";
            return RedirectToAction("Index");
        }

        public IActionResult Edit(Guid userId)
        {
            UserEditModel model = new UserEditModel();

            var user =_dBContext.User.Find(userId);
            if (user == null) return RedirectToAction("Index");

            model.userId = user.userId;
            model.username = user.username;

            model.authorizedPermissions = _dBContext.UserPermission.Where(x => x.userId == user.userId && x.isAuthorized).Select(x => x.permissionId.ToString().ToLower()).ToHashSet();

            return View("EditOtorisasi", model);
        }

        [HttpPost]
        public IActionResult UpdatePermission([FromBody] UpdatePermissionModel param)
        {
            if (!ModelState.IsValid) return BadRequest("Model state invalid");

            var userPermission = _dBContext.UserPermission.Where(x => x.userId == param.userId && x.permissionId == param.permissionId).FirstOrDefault();
            if (userPermission == null)
            {
                _dBContext.UserPermission.Add(new Entity.UserPermission()
                {
                    userPermissionId = Guid.NewGuid(),
                    userId = param.userId,
                    permissionId = param.permissionId,
                    isAuthorized = param.isAuthorized
                });
            } else
            {
                userPermission.isAuthorized = param.isAuthorized;
            }

            _dBContext.SaveChanges();

            return Ok();
        }

        public IActionResult ChangePassword()
        {
            var userAuth = (UserAuthModel)HttpContext.Items["User"];
            return View(new ChangePasswordModel()
            {
                userId = userAuth.userId.ToString()
            });;
        }

        [HttpPost]
        public IActionResult ChangePassword(ChangePasswordModel param)
        {
            if (ModelState.IsValid)
            {
                var user = _dBContext.User.Find(Guid.Parse(param.userId));
                if (user == null)
                {
                    TempData["errorMessage"] = "User tidak ditemukan";
                    return View();
                }

                if (!BCrypt.Net.BCrypt.Verify(param.oldPassword, user.password))
                {
                    TempData["errorMessage"] = "Password Lama salah";
                    return View();
                }
                
                user.password = BCrypt.Net.BCrypt.HashPassword(param.newPassword);
                _dBContext.SaveChanges();

                return RedirectToAction("Login", "User");
            }

            return View();
        }

        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Login(AuthenticateRequest param)
        {
            if(!ModelState.IsValid)
            {
                TempData["ErrorMessage"] = "Login gagal";
                return View();
            }

            var auth = _authorizationService.Authenticate(param);
            if(auth == null) {
                TempData["ErrorMessage"] = "Gagal login.";
                return View();
            }

            HttpContext.Session.SetString("JWToken", auth.token);

            return RedirectToAction("Index", "Main");
        }

        public IActionResult Logout()
        {
            HttpContext.Session.Clear();

            return RedirectToAction("Login", "User");
        }
    }
}
