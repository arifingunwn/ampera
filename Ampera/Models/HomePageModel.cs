﻿namespace Ampera.Models
{
    public class HomePageModel
    {
        public List<HomePageApiIdListItem> homePageApiIdListItems { get; set; } = new List<HomePageApiIdListItem>();
        public bool FeatApiStatusOK { get; set; }
    }

    public class HomePageApiIdListItem
    {
        public int ApiId { get; set; }
        public string ApiName { get; set; }
    }
}
