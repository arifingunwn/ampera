﻿namespace Ampera.Models
{
    public class OmzetCustomerViewModel
    {
        public DateTime awal { get; set; } 
        public DateTime akhir { get; set; }
        public string sortOrder { get; set; }
        public int rowCount { get; set; }
        public IEnumerable<OmzetCustomerItemModel> Items { get; set; } = new List<OmzetCustomerItemModel>();
        
        public IEnumerable<Microsoft.AspNetCore.Mvc.Rendering.SelectListItem> SortOrderOptions { get; set; } = new List<Microsoft.AspNetCore.Mvc.Rendering.SelectListItem>()
        {
            new Microsoft.AspNetCore.Mvc.Rendering.SelectListItem("Terbesar -> Terkecil", "DESC"),
            new Microsoft.AspNetCore.Mvc.Rendering.SelectListItem("Terkecil -> Terbesar", "ASC"),
        };
    }

    public class OmzetCustomerItemModel
    {
        public string kodeRekan { get; set; }
        public string namaRekan { get; set; }
        public string kodeSales { get; set; }
        public string namaSales { get; set; }
        public decimal totalOmzet { get; set; }
    }
}
