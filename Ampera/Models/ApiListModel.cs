﻿using Ampera.Entity.Feat;

namespace Ampera.Models
{
    public class ApiListModel
    {
        public List<ApiList> apiLists { get; set; }
    }
}
