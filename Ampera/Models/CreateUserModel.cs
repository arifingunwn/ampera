﻿using System.ComponentModel.DataAnnotations;

namespace Ampera.Models
{
    public class CreateUserModel
    {
        [Required(ErrorMessage = "Username harus diisi")]
        [StringLength(20, MinimumLength = 3, ErrorMessage = "Username Min 3 karakter Max 20 karakter")]
        public string username { get; set; }
        [Required]
        [StringLength(20, MinimumLength = 8, ErrorMessage = "Password Min 8 karakter Max 20 karakter")]
        public string password { get; set; }
        [Required]
        [StringLength(20, MinimumLength = 8, ErrorMessage = "Password Min 8 karakter Max 20 karakter")]
        [Compare("password", ErrorMessage = "Password Confirmation harus sama")]
        public string passwordConfirmation { get; set; }
    }
}
