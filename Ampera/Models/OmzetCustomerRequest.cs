﻿namespace Ampera.Models
{
    public class OmzetCustomerRequest
    {
        public DateTime awal { get; set; }
        public DateTime akhir { get; set; }
        public string sortOrder { get; set; }
        public int rowCount { get; set; }
    }
}
