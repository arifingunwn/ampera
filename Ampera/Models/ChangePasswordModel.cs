﻿using System.ComponentModel.DataAnnotations;

namespace Ampera.Models
{
    public class ChangePasswordModel
    {
        [Required]
        public string userId { get; set; }
        [Required]
        public string oldPassword { get; set; }
        [Required]
        [StringLength(int.MaxValue, MinimumLength = 8, ErrorMessage = "Password Min 8 karakter")]
        public string newPassword { get; set; }
        [Required]
        [StringLength(int.MaxValue, MinimumLength = 8, ErrorMessage = "Password Min 8 karakter")]
        [Compare("newPassword", ErrorMessage = "Password Confirmation harus sama")]
        public string newPasswordConfirmation { get; set; }
    }
}
