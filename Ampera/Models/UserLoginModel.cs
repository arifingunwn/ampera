﻿using System.ComponentModel.DataAnnotations;

namespace Ampera.Models
{
    public class UserLoginModel
    {
        [Required]
        public string username { get; set; }
        [Required]
        public string password { get; set; }
    }
}
