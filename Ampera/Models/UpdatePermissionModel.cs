﻿using System.ComponentModel.DataAnnotations;

namespace Ampera.Models
{
    public class UpdatePermissionModel
    {
        [Required]
        public Guid userId { get; set; }
        [Required]
        public Guid permissionId { get; set; }

        [Required]
        public bool isAuthorized { get; set; }
    }
}
