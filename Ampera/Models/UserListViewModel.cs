﻿namespace Ampera.Models
{
    public class UserListViewModel
    {
        public IEnumerable<UserListViewItemModel> users { get; set; } = new List<UserListViewItemModel>();
    }

    public class UserListViewItemModel
    {
        public string userId { get; set; }
        public string username { get; set; }
    }
}
