﻿namespace Ampera.Models
{
    public class SalesDailyReportModel
    {
        public DateTime awal { get;set;}
        public DateTime akhir { get;set;}
        public HashSet<string> DropDownListFilterSales { get; set; } = new HashSet<string>();
        public HashSet<string> DropDownListFilterRekan { get; set; } = new HashSet<string>();
        public List<SalesDailyReportSegmentModel> segments { get; set; } = new List<SalesDailyReportSegmentModel>();
        public decimal grandTotalOfOwner { get; set; } = 0;
        public decimal grandTotalOfYozuri { get; set; } = 0;
        public decimal grandTotalOfYZR { get; set; } = 0;
        public decimal grandTotalOfSmartChoice { get; set; } = 0;
        public decimal grandTotalOfOther { get; set; } = 0;
        public decimal grandTotalOfRetur { get; set; } = 0;
        public decimal grandTotalOfOmzet { get; set; } = 0;
        public decimal grandTotal { get; set; } = 0;
    }

    public class SalesDailyReportSegmentModel
    {
        public List<SalesDailyReportItemModel> items { get; set; } = new List<SalesDailyReportItemModel>();
        public decimal subTotalOfOwner { get; set; } = 0;
        public decimal subTotalOfYozuri { get; set; } = 0;
        public decimal subTotalOfYZR { get; set; } = 0;
        public decimal subTotalOfSmartChoice { get; set; } = 0;
        public decimal subTotalOfOther { get; set; } = 0;
        public decimal subTotalOfRetur { get; set; } = 0;
        public decimal subTotalOfOmzet { get; set; } = 0;
        public decimal subTotal { get; set; } = 0;
    }

    public class SalesDailyReportItemModel
    {
        public string kodeRekan { get; set; }
        public string namaRekan { get; set; }
        public string kodeSales { get;set; }
        public string namaSales { get; set; }
        public decimal sumOfOwner { get; set; } = 0;
        public decimal sumOfYozuri { get; set; } = 0;
        public decimal sumOfSmartChoice { get; set; } = 0;
        public decimal sumOfYZR { get; set; } = 0;
        public decimal sumOfOther { get; set; } = 0;
        public decimal sumOfRetur { get; set; } = 0;
        public decimal sumOfOmzet { get; set; } = 0;
        public decimal sumOfTotal { get; set; } = 0;
    }
}
