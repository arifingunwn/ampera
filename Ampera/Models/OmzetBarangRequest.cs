﻿namespace Ampera.Models
{
    public class OmzetBarangRequest
    {
        public DateTime awal { get; set; }
        public DateTime akhir { get; set; }
        public string namaBarang { get; set; } = string.Empty;
        public string sortBy { get; set; }
        public string sortOrder { get; set; }
        public int rowCount { get; set; }
    }
}
