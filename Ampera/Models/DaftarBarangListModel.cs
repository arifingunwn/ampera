﻿namespace Ampera.Models
{
    public class DaftarBarangListModel
    {
        public List<DaftarBarangModel> dataBarangList;
    }

    public class DaftarBarangModel
    {
        public string kodeBarang { get; set; }
        public string namaBarang { get; set; }
        public string satuan { get; set; }
        public decimal nilaiTambahan { get; set; } = 0;
    }
}
