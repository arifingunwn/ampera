﻿namespace Ampera.Models
{
    public class OnTimePaymentIndexViewModel
    {
        public DateTime awal { get; set; }
        public DateTime akhir { get; set; }
        public List<OnTimePaymentIndexViewItemModel> Items { get; set; } = new List<OnTimePaymentIndexViewItemModel>();
    }

    public class OnTimePaymentIndexViewItemModel
    {
        public string NoBukti { get; set; }
        public string kodeRekan { get; set; }
        public string namaRekan { get; set; }
        public DateTime tanggalBayar { get; set; }
        public string noFaktur { get; set; }
        public DateTime tanggalFaktur { get; set; }
        public decimal totalPenjualan { get; set; }
        public string noRetur { get; set; }
        public decimal totalRetur { get; set; }
        public string noTagihan { get; set; }
        public decimal totalTagihan { get; set; }
        public decimal bayar { get; set;}
        public decimal piutang { get; set; }

    }
}
