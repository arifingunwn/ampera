﻿namespace Ampera.Models
{
    public class AwalAkhirRequestParameter
    {
        public string awal { get; set; }
        public string akhir { get; set; }
    }
}
