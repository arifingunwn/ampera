﻿namespace Ampera.Models
{
    public class OmzetBarangViewModel
    {
        public DateTime awal { get; set; }
        public DateTime akhir { get; set; }
        public string namaBarang { get; set; }
        public string sortBy { get; set; }
        public string sortOrder { get; set; }
        public int rowCount { get; set; }
        public List<OmzetBarangItemModel> Items { get; set; } = new List<OmzetBarangItemModel>();

        public IEnumerable<Microsoft.AspNetCore.Mvc.Rendering.SelectListItem> sortOrderOptions { get; set; } = new List<Microsoft.AspNetCore.Mvc.Rendering.SelectListItem>()
        {
            new Microsoft.AspNetCore.Mvc.Rendering.SelectListItem("Terbesar -> Terkecil", "DESC"),
            new Microsoft.AspNetCore.Mvc.Rendering.SelectListItem("Terkecil -> Terbesar", "ASC"),
        };

        public IEnumerable<Microsoft.AspNetCore.Mvc.Rendering.SelectListItem> sortByOptions { get; set; } = new List<Microsoft.AspNetCore.Mvc.Rendering.SelectListItem>()
        {
            new Microsoft.AspNetCore.Mvc.Rendering.SelectListItem("Laba", "LABA"),
            new Microsoft.AspNetCore.Mvc.Rendering.SelectListItem("Omzet", "OMZET"),
            new Microsoft.AspNetCore.Mvc.Rendering.SelectListItem("Quantity", "QUANTITY"),
        };
    }

    public class OmzetBarangItemModel
    {
        public string kodeBarang { get; set; }
        public string namaBarang { get; set; }
        public decimal qty { get; set; } = 0;
        public string satuan { get;set; }
        public decimal labaAwal { get; set; } = 0;
        public decimal modalTambahan { get; set; } = 0;
        public decimal labalAkhir { get; set; } = 0;
        public decimal omzet { get; set; } = 0;

    }
}
