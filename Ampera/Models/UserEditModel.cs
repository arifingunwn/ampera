﻿namespace Ampera.Models
{
    public class UserEditModel
    {
        public Guid userId { get; set; }
        public string username { get; set; }
        public HashSet<string> authorizedPermissions { get; set; } = new HashSet<string>();
    }
}
