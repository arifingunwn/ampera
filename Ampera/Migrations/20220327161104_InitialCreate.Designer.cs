﻿// <auto-generated />
using System;
using Ampera;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

#nullable disable

namespace Ampera.Migrations
{
    [DbContext(typeof(AmperaDBContext))]
    [Migration("20220327161104_InitialCreate")]
    partial class InitialCreate
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder.HasAnnotation("ProductVersion", "6.0.3");

            modelBuilder.Entity("Ampera.Entity.ModalTambahan", b =>
                {
                    b.Property<string>("kodeBarang")
                        .HasColumnType("TEXT");

                    b.Property<decimal>("nilaiTambahan")
                        .HasColumnType("TEXT");

                    b.Property<DateTime>("updatedDate")
                        .HasColumnType("TEXT");

                    b.HasKey("kodeBarang");

                    b.ToTable("ModalTambahan");
                });
#pragma warning restore 612, 618
        }
    }
}
