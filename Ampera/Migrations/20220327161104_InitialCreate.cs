﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Ampera.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ModalTambahan",
                columns: table => new
                {
                    kodeBarang = table.Column<string>(type: "TEXT", nullable: false),
                    nilaiTambahan = table.Column<decimal>(type: "TEXT", nullable: false),
                    updatedDate = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ModalTambahan", x => x.kodeBarang);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ModalTambahan");
        }
    }
}
