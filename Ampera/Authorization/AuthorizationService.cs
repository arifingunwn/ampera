﻿using Ampera.Authorization;
using Ampera.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using BCryptNet = BCrypt.Net.BCrypt;

namespace Ampera
{
    public class AuthorizationService : IAuthorizationService
    {
        private readonly AuthorizationSettings _appSettings;
        private readonly AmperaDBContext _dbContext;
        public AuthorizationService(IOptions<AuthorizationSettings> appSettings, AmperaDBContext dbContext)
        {
            _appSettings = appSettings.Value;
            _dbContext = dbContext; 
        }
        public AuthenticateResponse Authenticate(AuthenticateRequest model)
        {
            var user = _dbContext.User.Where(x => x.username == model.username).FirstOrDefault();
            if (user == null) return null;

            if (!BCrypt.Net.BCrypt.Verify(model.password, user.password)) return null;
                

            var userPermissions = _dbContext.UserPermission.Where(x => x.userId == user.userId)?.Select(x => x.permissionId).ToHashSet();

            var token = generateJwtToken(user.userId, user.username, userPermissions);

            return new AuthenticateResponse
            {
                userId = user.userId,
                username = user.username,
                permissions = userPermissions,
                token = token,
            };
        }

        private string generateJwtToken(Guid userId, string username, IEnumerable<Guid> permissionIds)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[] {
                    new Claim("userId", userId.ToString()),
                    new Claim("username", username),
                    new Claim("permissionIds", JsonConvert.SerializeObject(permissionIds))
                }),
                Expires = DateTime.UtcNow.AddDays(1),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }


    }
}
