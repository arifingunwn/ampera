﻿namespace Ampera.Authorization
{
    public class AuthenticateResponse
    {
        public Guid userId { get; set; }
        public string username { get; set; }
        public HashSet<Guid> permissions { get; set; } = new HashSet<Guid>();
        public string token { get; set; }
    }
}
