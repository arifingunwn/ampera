﻿using System.ComponentModel.DataAnnotations;

namespace Ampera.Authorization
{
    public class AuthenticateRequest
    {
        [Required]
        public string username { get; set; }
        [Required]
        public string password { get; set; }
    }
}
