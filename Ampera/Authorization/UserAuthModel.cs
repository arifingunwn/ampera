﻿namespace Ampera.Authorization
{
    public class UserAuthModel
    {
        public Guid userId { get; set; }
        public string username { get; set; }
        public HashSet<string> permissions { get; set; } = new HashSet<string>();

        public UserAuthModel(Guid userId, string username, HashSet<string> permissions)
        {
            this.userId = userId;
            this.username = username;
            this.permissions = permissions;
        }
    }
}
