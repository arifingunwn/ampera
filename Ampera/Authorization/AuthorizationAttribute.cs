﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Ampera.Authorization
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class AuthorizationAttribute : Attribute, IAuthorizationFilter
    {
        public string PermissionId { get; set; } = string.Empty;
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            var user = (UserAuthModel)context.HttpContext.Items["User"];
            if(user == null)
            {
                redirectToLogin(context);
                return;
            }

            if(PermissionId != string.Empty)
            {
                if(user.permissions.Count <= 0) redirectToLogin(context);
                if (!user.permissions.Contains(PermissionId))
                {
                    redirectToLogin(context);
                }
            }
        }

        private void redirectToLogin(AuthorizationFilterContext context)
        {
            context.Result = new RedirectToActionResult("Login", "User", null);
        }
    }
}
