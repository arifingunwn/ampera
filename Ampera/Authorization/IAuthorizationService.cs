﻿using Ampera.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ampera
{
    public interface IAuthorizationService
    {
        AuthenticateResponse Authenticate(AuthenticateRequest model);
    }

}
