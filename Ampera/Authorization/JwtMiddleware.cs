﻿using Ampera.Authorization;
using Ampera.Services;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using System.IdentityModel.Tokens.Jwt;
using System.Text;

namespace Ampera.Authorization
{
    public class JwtMiddleware
    {
        private readonly RequestDelegate _requestDelegate;
        private readonly AuthorizationSettings _appSettings;

        public JwtMiddleware(RequestDelegate requestDelegate, IOptions<AuthorizationSettings> appSetting)
        {
            _requestDelegate = requestDelegate;
            _appSettings = appSetting.Value;
        }

        public async Task Invoke(HttpContext context)
        {
            var token = context.Session.GetString("JWToken");

            if (token != null)
            {
                attachUserToContext(context, token);
            }

            await _requestDelegate(context);
        }

        private void attachUserToContext(HttpContext context, string token)
        {
            try
            {
                var tokenHandler = new JwtSecurityTokenHandler();
                var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
                tokenHandler.ValidateToken(token, new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    // set clockskew to zero so tokens expire exactly at token expiration time (instead of 5 minutes later)
                    ClockSkew = TimeSpan.Zero
                }, out SecurityToken validatedToken);

                var jwtToken = (JwtSecurityToken)validatedToken;
                var userId = Guid.Parse(jwtToken.Claims.First(x => x.Type == "userId").Value);
                var username = (string)jwtToken.Claims.First(x => x.Type == "username").Value;
                var strPermissionIds = (string)jwtToken.Claims.First(x => x.Type == "permissionIds").Value;
                var permissionIds = JsonConvert.DeserializeObject<HashSet<string>>(strPermissionIds);


                // attach user to context on successful jwt validation
                context.Items["User"] = new UserAuthModel(userId, username, permissionIds);
            }
            catch
            {
                // do nothing if jwt validation fails
                // user is not attached to context so request won't have access to secure routes
            }
        }
    }
}
