﻿using Ampera.Authorization;

namespace Ampera
{
    public static class AuthorizationExtention
    {
        public static bool HasPermission(this HttpContext context, string permissionId)
        {
            if (!context.Items.ContainsKey("User")) return false;

            var usr = (UserAuthModel)context.Items["User"];

            return usr.permissions.Contains(permissionId);
        }
    }  
}
